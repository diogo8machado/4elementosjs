import {newElement} from './func'

export function about(home){
    //criando div raiz do sobre(about) e vinculando-o a div pai(home) 
    const aboutBox = newElement('div','aboutBox bg','about','',home);
    //div com estilo container
    const aboutContainer = newElement('div','container py-5','aboutBoxContainer','',aboutBox);
    //Titulo
    newElement('h1','title white py-5','','Sobre nós',aboutContainer);

    //Texto do Sobre
    const aboutText='A Quatro Elementos é a Empresa Júnior do curso de Administração da UFERSA, que busca proporcionar a vivência empresarial para seus membros e inserir no mercado profissionais mais capacitados. A empresa trabalha atualmente com um portfólio contendo 13 serviços, divididos em cinco áreas de soluções em Administração, sendo elas: Finanças, Gestão de pessoas, Estratégia, Marketing e Processos. A atividade fim da Quatro Elementos é o serviço de consultoria e assessoria empresarial. Inserida em um contexto de crescimento, nossos serviços tratam-se de projetos realizados em tempo determinados, desempenhados pela equipe de consultores, que contam com o apoio dos professores mestres e doutores com vasta experiência de mercado que realizam seu acompanhamento.'
    newElement('p','text white pb-5','',aboutText,aboutContainer);
}