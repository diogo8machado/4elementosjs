import {newElement} from './func'
import logo from '../img/logo.svg'

export function imgLogo(home){
    //criando div raiz da Imagem Principal(imgLogo) e vinculando-o a div pai(home) 
    const imgBox = newElement('div','imgBox container','imgBox','',home);
    //Imagem
    newElement('img','imgLogo','imgLogo','',imgBox,logo,'4 Elementos')
}