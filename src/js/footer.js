import FaceIcon from '../img/FaceIcon.svg'
import InstaIcon from '../img/InstaIcon.svg'
import WppIcon from '../img/WppIcon.svg'
import PointIcon from '../img/PointIcon.svg'
import CellIcon from '../img/CellIcon.svg'
import EmailIcon from '../img/EmailIcon.svg'
import whiteLogo from '../img/WhiteLogo.svg'

import {newElement} from './func'

export function footer(home){
    //criando div raiz do Rodapé(footer) e vinculando-o a div pai(home) 
    const footerBg = newElement('div','footerBg','footer','',home);
    //div com estilo container
    const footerContainer = newElement('div','container py-5','footerContainer','',footerBg);

    //Área para os contatos e a logo
    const footerBox = newElement('div','footerBox','','',footerContainer);

    //Área dos contatos
    const contactsBox = newElement('div','contactsBox','','',footerBox);

    //Dados da empressa
    const contactsList=[
        {icon:CellIcon,text:'(84) 99857-5095'},
        {icon:PointIcon,text:'R. Francisco Mota - Pres. Costa e Silva, Mossoró - RN, 59625-620'},
        {icon:EmailIcon,text:'email@email.com'},
    ]

    //laço para cria os 3 dados e seu respectivo icone
    contactsList.forEach((item)=>{
        //Área de um contato
        const contactsFooter = newElement('div','contactsFooter','','',contactsBox);
        //O icone
        newElement('img','iconCont','','',contactsFooter,item.icon,'');
        //Texto
        newElement('p','textFooter white','',item.text,contactsFooter);
    })
    //Texto das redes sociais
    newElement('p','textSocial white','','Siga-nos nas redes sociais!',contactsBox);

    //Área das redes sociais
    const contactsFooter = newElement('div','contactsFooter','','',contactsBox);
    newElement('img','iconCont','','',contactsFooter,FaceIcon,'');
    newElement('img','iconCont','','',contactsFooter,InstaIcon,'');
    newElement('img','iconCont','','',contactsFooter,WppIcon,'');

    //Logo da empressa branca
    const logoFooterBox = newElement('div','logoFooterBox','','',footerBox);  
    newElement('img','whiteLogo','','',logoFooterBox,whiteLogo,'');

   }