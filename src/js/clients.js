import {newElement} from './func'
import vprImg from '../img/vpr.svg'
import mhfImg from '../img/mhf.svg'
import betaImg from '../img/beta.svg'

export function clients(home){
    //Criando div raiz dos Clientes(clients) e vinculando-o a div pai(home) 
    const clients = newElement('div','container clients','clients','',home);
    //Titulo
    newElement('h1','title titleClient blue','','Inove como eles',clients);
    //Div para agrupar as logos
    const logosClients = newElement('div','logosClients','logosClients','',clients);

    //As 3 logos do clientes
    newElement('img','vprImg','','',logosClients,vprImg,'');
    newElement('img','mhfImg','','',logosClients,mhfImg,'');
    newElement('img','betaImg','','',logosClients,betaImg,'');
}