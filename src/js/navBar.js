
import {newElement} from './func'
import logoImg from '../img/logo4.svg'
import wppIcon from '../img/WppIcon.svg'

export function navBar(){
    //Criando a Barra de navegação
    const root=document.querySelector('#root');
    const navBar=newElement('nav','navBar','nav','',root);
    
    //Adicionando estilo Container
    const contNav= newElement('div','container contNav','contNav','',navBar);

    //Adicionando logo no canto esquerdo da barra
    const logo= newElement('a','logoBox','logo','',contNav);
    logo.href='#imgLogo'
    newElement('img','logo4','logo4','',logo,logoImg,'4 logo');

    //Adicionando botão collaps para telas pequenas
    const collaps= newElement('div','collaps','collaps','',contNav);
    const buttoncollaps= newElement('button','buttoncollaps','buttoncollaps','',collaps);
    newElement('span','spanCollaps','','',buttoncollaps);

    //Adcionando area do menu a direita da barra
    const menu= newElement('div','menu','menu','',contNav);
    const navList= newElement('ul','navList','navList','',menu);

    //Lista dos Link de navegação 
    const menuList=[{content:'Home',href:'#imgLogo'},{content:'Sobre',href:'#about'}
    ,{content:'Serviços',href:'#services'},{content:'Clientes',href:'#clients'},
    {content:'Contatos',href:'#contact'}]
    menuList.forEach((item)=>{
        const navLiItem= newElement('li','navLi','','',navList);
        const navAItem= newElement('a','navText','',item.content,navLiItem);
        navAItem.href=item.href
    })

    //Botão que redireciona para conversa no whatapps com a empressa
    const navLiWPP= newElement('li','navLi','wpp','',navList);
    const navAWPP= newElement('a','wppButton','','',navLiWPP);
    newElement('img','wppIcon','','',navAWPP,wppIcon,'');
    const boxWpp= newElement('div','','','',navAWPP);
    newElement('a','wppButtonText','','Whatsapp',boxWpp);
    navAWPP.href='#imgLogo'

   

}