import {newElement} from './func'
import bookImg from '../img/books.svg'

export function books(home){
    //criando div raiz dos E-books(books) e vinculando-o a div pai(home) 
    const books = newElement('div','bg','books','',home);
    //div com estilo container
    const bookContainer = newElement('div','container py-5 bookContainer','','',books);
    //Titulo
    newElement('h1','title white py-5','','Descubra soluções para sua empresa!',bookContainer);
    //Imagem dos livros
    newElement('img','booksImg','booksImg','',bookContainer,bookImg,'Books');
    //Texto
    const booksText='Inscreva-se para receber esses e-Books e mais dicas para sua empresa'
    newElement('h3','textBook text white ','',booksText,bookContainer);

    //div com estilos para o input e o button
    const inputContainer=newElement('div','inputContainer','','',bookContainer);
    //div com estilos para o input
    const inputBox=newElement('div','inputBox','','',inputContainer);

    //input para pegar o E-mail do usuario
    const input=newElement('input','form-control','emailBooks','',inputBox);
    input.placeholder='Email...';
    //Button para confirmar a ação
    const buttonBox=newElement('div','buttonBox','','',inputContainer);;
    newElement('button','btn buttonBooks','submitEmail','Enviar',buttonBox);
}