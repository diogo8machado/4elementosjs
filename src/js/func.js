const URL = "4elementos.diogomachado.site" //quando testar altera para 'http://http://localhost:9000/'
import axios from 'axios'
//função criar elemento, tem como objetivo receber parametro para ciar uma tag html e adiciona-lo a algum elemento pai
//Os parametros podem ser a tag, a classe, o id, o conteudo, o elemento pai, um link da imagem e o alt dela
  function newElement(tagName='', className='', id='',content='',elemParent,src='',alt='') {
      //Cria um elemento com a tag
    const elem = document.createElement(tagName)
    //Verifica se tem alguma classe, caso tenha adciona, caso contrario passa para o proximo 
    if(className!==''){
        elem.className = className
    }
    //Verifica se tem alguma] ID
    if(id!==''){
        elem.id = id
    }
    //Verifica se tem algum conteudo 
    if(content!==''){
        elem.innerHTML=content
    }
    //Verifica se tem algum Link
    if(src!==''){
        elem.src=src
    }
    //Verifica se tem algum alt
    if(alt!==''){
        elem.alt=alt
    }
    //Adiciona ao elemento pai
    elemParent.appendChild(elem);
    //retorna o elemento, caso seja preciso.
    return elem

}
function navbarAnimation() {
    let lastTest=false;//varivel que evita repetições desnecessaria nas tag, cujo possuem a mesma classe
    window.addEventListener('scroll', (e) => {//evento para acompanhar o scroll

        let last_known_scroll_position = window.scrollY;//pegar a posição do top da pagina
        let ticking
        if (!ticking) {
            window.requestAnimationFrame(() => {
                if ((last_known_scroll_position + 3) >= getPos('about').y) {//Quando estiver abaixo da div #about
                    if (lastTest !== true) {// caso tenha passado da posição e antes não tivesse passado inicia o processo para ele aparecer 
                        lastTest = true //define como verdadeiro para evitar que na proxima vez esse efeito nao se repita 
                        document.getElementById("nav").classList.add("navActive");//adiciona as classe
                        document.getElementById("home").classList.add("home");
                    }
                } else {
                    if (lastTest !== false) {//Neste caso é para quando for um posição acima da div #about
                        lastTest = false;
                        document.getElementById("nav").classList.add("desaparecer");//ativa a classe para animação
                       setTimeout(() => {//função para evitar que a navBar desapareça antes da animação terminar 
                            document.getElementById("nav").classList.remove("desaparecer");
                            document.getElementById("nav").classList.remove("navActive");
                            document.getElementById("home").classList.remove("home");
                        }, 500);
                    }


                }

                ticking = false;
            });
        }
        ticking = true;
    })
}
//função para dar enfeito no scroll para navegar entre as seções
function scrolling() {
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();
            document.querySelector(this.getAttribute('href')).scrollIntoView({//animação
                behavior: 'smooth'
            });
        });
    });
}
//função para aparecer menu
function collaps() {
    document.getElementById("buttoncollaps").addEventListener("click", function () {

        document.getElementById("menu").classList.toggle("active");//adiciona ou remove a class
    });
}
function getPos(id) {//pega a possição de uma div
    let el
    el = document.getElementById(id)
    return { x: (el.offsetHeight+el.offsetTop), y: el.offsetTop }
}

function sendEmail(){//envia os dados para a api
    document.getElementById("submitEmail").addEventListener("click", function () {
        const value=document.getElementById("emailBooks").value
        document.getElementById("submitEmail").disabled = true; //desabilita o botão enquanto não recebe resposta
        axios.post(`api-${URL}/inscrever`, { email:value }).then(()=>{ //caso tenha obtido sucesso na api, retorna para o estado inicia o input e o botão, e mostra uma notificição de email enviado 
            document.getElementById("emailBooks").value=''
            document.getElementById("submitEmail").disabled = false; 
            toastr('Email enviado com sucesso','sucess') //notificação de sucesso
        }).catch(e =>{
            //caso dê erro envia uma menssagem 
                    toastr('Erro: E-mail invalido','error')
            document.getElementById("submitEmail").disabled = false; 
           
        })
        
    });  
}
//segue o mesmo padrao do sendEmail porem adicionando outros dados e o request para /contato
function contactEmail(){
    document.getElementById("buttonContact").addEventListener("click", function () {
        const name=document.getElementById("nome").value
        const email=document.getElementById("email").value
        const phone=document.getElementById("telefone").value
        const message=document.getElementById("textArea").value
        document.getElementById("buttonContact").disabled = true; 
        axios.post(`api-${URL}/contato`, { name,email,phone,message }).then(()=>{ 
            document.getElementById("nome").value=''
            document.getElementById("email").value=''
            document.getElementById("telefone").value=''
            document.getElementById("textArea").value=''
            document.getElementById("buttonContact").disabled = false; 
            toastr('Email enviado com sucesso','sucess')
        }).catch(e =>{
            console.log(e.response.data)
            toastr('Erro: E-mail invalido','error')
            document.getElementById("buttonContact").disabled = false; 
           
        })
        
    });  
}

//função para envar notificação
function toastr(mensagem,type){//recebe a mensagem e vê positiva ou negativa
    const toastrBox = document.querySelector('#toastrBox')
   
    const toastr = newElement('div', 'toastr', 'toastr', mensagem, toastrBox);
    if(type==='sucess'){
        toastr.classList.add("toastrSucess");//adiciona a classe
    }else{
        toastr.classList.add("toastrError");
    }
    setTimeout(() => {//tempo para aparecer
        toastr.classList.add("toastrActive");
    }, 100);
    setTimeout(() => { //tempo para permanecer
        toastr.classList.remove("toastrActive");
        setTimeout(() => {//tempo para desaparecer
            const node = document.getElementById("toastr");
            node.parentNode.removeChild(node);
        }, 600);
    }, 2000);
    
}

export {newElement,scrolling,collaps,navbarAnimation,sendEmail,contactEmail}