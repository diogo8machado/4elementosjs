import {newElement} from './func'
import Pessoas from '../img/Pessoas.svg'
import Financas from '../img/Financas.svg'
import Processos from '../img/Processos.svg'
import Marketing from '../img/Marketing.svg'
import Gestao from '../img/Gestao.svg'

export function services(home){
    //Área dos serviços
    const services = newElement('div','container py-5','services','',home);
    //Titulo
    newElement('h1','title blue py-5','','Serviços',services);
    //Área dos serviços
    const servicesBox = newElement('div','servicesBox','servicesBox','',services);

    //Lista dos serviços
    let servicos=[
        {title:'Gestão de Pessoas',image:Pessoas,servicos:['Descrição de cargos','Avaliação de desempenho','Recrutamento e seleção','Motivação de funcionários' ]},
        {title:'Finanças',image:Financas,servicos:['Plano Financeiro','Redução de custos','Análise dos Indicadores de Desempenho']},
        {title:'Processos',image:Processos,servicos:['Otimização dos processos','Fluxograma']},
        {title:'Marketing',image:Marketing,servicos:['Plano de marketing','Pesquisa de marketing','E-marketing "sua empresa nas mídias digitais"']},
        {title:'Gestão',image:Gestao,servicos:['Plano de negócio, visa orientar na viabilidade da ideia e gestão da empresa.']},
    ]   
    //Criando os cards
    servicos.forEach((item,index) =>{
        card(servicesBox,item,index)
    }) 
}

function card(servicesBox,item,index){
    //Area
    const servicesCard = newElement('div',`servicesCard `,'','',servicesBox);
    //O Card
    const servicesBg = newElement('div',`servicesBg service${index+1}`,`service${index+1}`,'',servicesCard);
    //Imagem do tipo de serviço
    newElement('img','imgService','','',servicesBg,item.image,'')
    //Titulo do tipo de serviço
    newElement('h2','titleCard white','',item.title,servicesBg);

    
    const ulCard=newElement('ul','ulCard','','',servicesBg);
    //Laços para criar os serviços
    item.servicos.forEach((item)=>{
        newElement('li','textCard white','',item,ulCard);
    })
}
