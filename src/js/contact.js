import {newElement} from './func'

export function contact(home){
    //criando div raiz dos contatos(contact) e vinculando-o a div pai(home) 
    const contactBg = newElement('div','contactBg bg','contact','',home);
    const contactContainer = newElement('div','container py-5','contactContainer','',contactBg);
    //Titulo
    newElement('h1','titleContact title white','','Contatos',contactContainer);
    //Texto 
    const contactText='Ficou interessado ou tem alguma duvida?'
    newElement('p','contactText white pb-5','',contactText,contactContainer);
    newElement('p','contactText white pb-5','','Fale conosco!',contactContainer);
    //div para área do formulario e do mapa
    const contactBox = newElement('div','contactBox','','',contactContainer);

    //Área do formulario
    const formBox = newElement('div','formBox','','',contactBox);
   
    //Nome
    const inputNome=newElement('input','form-control marginForm','nome','',formBox);
    inputNome.placeholder='Nome...';
    //E-mail
    const inputEmail=newElement('input','form-control marginForm','email','',formBox);
    inputEmail.placeholder='Email...';
    //Telephone
    const inputTelefone=newElement('input','form-control marginForm','telefone','',formBox);
    inputTelefone.placeholder='Telefone...';
    //Mensagem do cliente(duvida/problema) 
    const textArea=newElement('textArea','form-control marginForm textArea','textArea','',formBox);
    textArea.placeholder='Em que podemos te ajudar?';
    
    //botão de confirmação
    newElement('button','btn buttonForm','buttonContact','Enviar',formBox);
   
    //Área do mapa
    const mapBox = newElement('div','mapBox','','',contactBox);  
    //Mapa do google maps
    mapBox.innerHTML=`<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.246984984876!2d-37.32357218776622!3d-5.205857721040002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ba070a1be380d9%3A0x6e321120e2f23469!2sCentro+de+Conviv%C3%AAncia+-+Campus+Leste+UFERSA!5e0!3m2!1spt-BR!2sbr!4v1543709397293"frameborder="0"  allowfullscreen></iframe>`;
}