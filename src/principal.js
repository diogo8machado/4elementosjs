
import './css/main.scss'
import { newElement,scrolling,collaps,navbarAnimation,sendEmail,contactEmail } from './js/func'
import { navBar } from './js/navBar'
import { imgLogo } from './js/imgLogo'
import { about } from './js/about'
import { services } from './js/services'
import { books } from './js/books'
import { clients } from './js/clients'
import { contact } from './js/contact'
import { footer } from './js/footer'

//primeira chamada função
inicio()

//tem como objetivo iniciar as chamadas de funções que criarão a pagina, iniciamente chamasse duas funçõs
function inicio() {
    //dividindo a pagina em dois, teremos a navBar(barra de navegação) e a home(a pagina propriamente dita)
    navBar();//chamada para criar a navBar
    home();//chamada para criar o resto da pagina

    funcs();//Chamada adicionar os EventListener
    
}
function funcs(){
    scrolling()//usado para fazer a rolagem da pagina quando clicar em um link anchor da pagina
    collaps()//usado para mostrar o menu da navBar quando estiver eme telas pequenas
    navbarAnimation()//animação para a navBar aparecer e desaparecer 
    sendEmail()//Será ativado quado usuario inscrever-se na pagina
    contactEmail()//quando o usuario entra em contato.
}
//desenhar a pagina
function home() {
    //Selecionando a div raiz
    const root = document.querySelector('#root')
    //Criando a div Home
    const home = newElement('div', '', 'home', '', root);
    //adicionando a div para as notificações
    newElement('div', 'toastrBox', 'toastrBox','', home);
    //chamas para desenhas as partes da pagina
    imgLogo(home);
    about(home);
    services(home);
    books(home);
    clients(home);
    contact(home);
    footer(home);
    
}

